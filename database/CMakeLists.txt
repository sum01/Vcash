list(APPEND _database_sources
	block.cpp
	broadcast_operation.cpp
	compression.cpp
	ecdhe.cpp
	entry.cpp
	find_operation.cpp
	hc256.cpp
	key_pool.cpp
	message.cpp
	node.cpp
	node_impl.cpp
	operation.cpp
	operation_queue.cpp
	ping_operation.cpp
	query.cpp
	routing_table.cpp
	rpc.cpp
	slot.cpp
	stack.cpp
	stack_impl.cpp
	storage.cpp
	store_operation.cpp
	udp_handler.cpp
	udp_multiplexor.cpp
	whirlpool.cpp
)

#list(TRANSFORM _database_sources PREPEND "src/")
prepend_to_list("src/" _database_sources)

list(APPEND _database_headers
	block.hpp
	broadcast_operation.hpp
	byte_buffer.hpp
	compression.hpp
	constants.hpp
	cpu.hpp
	crypto.hpp
	ecdhe.h
	ecdhe.hpp
	ecrypt-config.h
	ecrypt-machine.h
	ecrypt-portable.h
	ecrypt-sync.h
	entry.hpp
	find_operation.hpp
	handler.hpp
	hc256.hpp
	key_pool.hpp
	logger.hpp
	message.hpp
	network.hpp
	node.hpp
	node_impl.hpp
	operation.hpp
	operation_queue.hpp
	ping_operation.hpp
	protocol.hpp
	query.hpp
	random.hpp
	routing_table.hpp
	rpc.hpp
	slot.hpp
	stack.hpp
	stack_impl.hpp
	storage.hpp
	storage_node.hpp
	store_operation.hpp
	udp_handler.hpp
	udp_multiplexor.hpp
	utility.hpp
	whirlpool.hpp
)

#list(TRANSFORM _database_headers PREPEND "src/")
prepend_to_list("src/" _database_headers)

# No static/shared since that's controlled by BUILD_SHARED_LIBS
add_library(database
	${_database_sources}
)

# Attaches headers to the lib
# Lets install commands install headers, among other things
set_target_properties(database PROPERTIES
	OUTPUT_NAME "vcash_database"
	PUBLIC_HEADER "${_database_headers}"
)

target_include_directories(database PUBLIC
	$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>
	$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
)

# <limits.h> requires C99
# C++11 is to make sure we compile with "modern" C++
target_compile_features(database
	PUBLIC
		c_std_99
		cxx_std_11
		cxx_auto_type
	PRIVATE
		cxx_range_for
)

# ecdhe.h uses openssl
# <thread> used in stack_impl.hpp
# network.hpp uses Boost system
# routing_table.cpp uses Boost timer
# Boost headers are used almost everywhere
target_link_libraries(database
	PUBLIC
		Boost::boost
		Boost::system
		Threads::Threads
		OpenSSL::SSL
		OpenSSL::Crypto
	PRIVATE
		Boost::timer
)

vcash_install_lib(database)

configure_package_config_file("Vcash-databaseConfig.cmake.in"
	"${CMAKE_CURRENT_BINARY_DIR}/Vcash-databaseConfig.cmake"
	INSTALL_DESTINATION "${_TARGET_INSTALL_CMAKEDIR}"
	# Doesn't use either macro
	NO_SET_AND_CHECK_MACRO
	NO_CHECK_REQUIRED_COMPONENTS_MACRO
)

install(FILES "${CMAKE_CURRENT_BINARY_DIR}/Vcash-databaseConfig.cmake"
	DESTINATION "${_TARGET_INSTALL_CMAKEDIR}"
)

if(VCASH_BUILD_EXE_DATABASE)
	add_subdirectory(bin)
endif()
