# This isn't needed in Cmake v3.12 because of list(TRANSFORM)
# but till v3.12 we need this...
function(prepend_to_list _prefix _listvar)
	# Go through each item in the list
	foreach(_list_item ${${_listvar}})
		# Prepend the prefix onto the current item
		list(APPEND _new_list "${_prefix}${_list_item}")
	endforeach()
	# Replace the old list with the fixed list
	set(${_listvar} ${_new_list} PARENT_SCOPE)
	# Make sure to clear out the temp var
	unset(_new_list)
endfunction()

# Installs a target (lib or exe)
# Also creates/installs exports for libs
function(vcash_install_lib _install_target)
	if(NOT TARGET ${_install_target})
		message(FATAL_ERROR "[vcash_install_lib error] You can't install \"${_install_target}\", it's not a target!")
	endif()

	# Get the type of the install target
	get_target_property(_target_type ${_install_target} TYPE)

	# Error-checking
	if(NOT _target_type MATCHES ".*LIBRARY")
		message(FATAL_ERROR "Something other than a library was passed to vcash_install_lib!")
	endif()

	# Gives "Make install" esque operations a location to install to...
	# and creates a .cmake file to be exported
	install(TARGETS ${_install_target}
		EXPORT "Vcash-${_install_target}Targets"
		LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}"
		ARCHIVE DESTINATION "${CMAKE_INSTALL_LIBDIR}"
		# Tells it where to put the header files
		# Example: /usr/include/vcash/<libname>/
		PUBLIC_HEADER DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/vcash/${_install_target}"
	)

	# "The install(TARGETS) and install(EXPORT) commands work together to install a target and a file to help import it"
	# Installs a cmake file which external projects can import.
	install(EXPORT "Vcash-${_install_target}Targets"
		NAMESPACE Vcash::
		DESTINATION "${_TARGET_INSTALL_CMAKEDIR}"
	)

	# "The export command is used to generate a file exporting targets from a project build tree"
	# Creates an import file for external projects which are aware of the build tree.
	# May be useful for cross-compiling
	export(TARGETS ${_install_target}
		FILE "Vcash-${_install_target}Exports.cmake"
	)
endfunction()
