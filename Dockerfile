# Artful has a combo of new Cmake with older Boost that works well for Vcash
FROM ubuntu:artful
MAINTAINER sum01 <sum01@protonmail.com>
RUN apt-get update \
	&& apt-get install -y \
			build-essential \
			cmake \
			libboost-all-dev \
			libdb++-dev \
			libdb-dev \
			libssl-dev \
			openssl \
	&& rm -rf /var/lib/apt/lists/*
COPY . /workdir
RUN mkdir -p /workdir/build && cd /workdir/build \
	&& cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_INSTALL_LIBDIR=lib .. \
	&& cmake --build . --target install -- -j$(nproc)
