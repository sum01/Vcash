# Vcash

A decentralized currency for the internet.

[![Gitter](https://badges.gitter.im/openvcash/vcash.svg)](https://gitter.im/openvcash/vcash?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

This project is a codebase rewrite/enhancment using:

* [Peercoin](https://github.com/ppcoin/ppcoin) PoS & [Bitcoin](https://github.com/bitcoin/bitcoin) PoW consensus
* an UDP layer
* an off-chain transaction lock voting system called ZeroTime
* an Incentive Reward voting system
* a client-side blending system called ChainBlender

## Dependencies

| Name        | Min    | Max    |
| :---------- | :----- | :----- |
| Boost       | 1.53.0 | 1.65.1 |
| Berkeley DB | 5.3.0  | 6.1.29 |
| OpenSSL     | 1.0.1  | 1.0.2  |

## Building

Cmake v3.8 (or higher)
